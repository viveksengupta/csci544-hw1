I am using this repository to do the CSCI 544 homeworks.
I will also get to learn Python and I am looking forward to it big time.
This repository could be accessed using HTTPS or SSH.
Currently I have set up the repository to use SSH keys, so that I don't have to enter my password.
The url value in the configuration file can be changed to make the repository access the HTTPS version.
If it indeed uses HTTPS, every time a git push is done, it will require the password.
